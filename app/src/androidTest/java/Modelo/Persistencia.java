package Modelo;

import com.armenta.preexamenunidadii.Usuario;
public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuario usuario);
}
